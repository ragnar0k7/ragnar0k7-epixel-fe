import styles from '../styles/Home.module.css'
import {signIn, signOut, useSession} from 'next-auth/client';

export default function Home() {
    const [session, loading] = useSession()

    return (
        <div className={styles.container}>
            <div>

                {!session && <>
                    <h2>Not signed in</h2> <br/>
                    <button onClick={() => signIn()}>Sign in</button>
                </>}
                {session && <>
                    <h2>Signed in as {session.user.name}</h2> <br/>
                    <button onClick={() => signOut()}>Sign out</button>
                </>}

            </div>
        </div>

    )
}
